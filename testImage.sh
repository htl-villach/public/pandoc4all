#!/bin/sh

if [ -z $1 ]; then
  echo "usage $0: imagename"
  exit 1
fi

docker run --rm -v $(pwd):/data  $1 testData/code-samples.md -o code-samples.pdf --listings --template code-printout.tex
docker run --rm -v $(pwd):/data  $1 testData/plantuml.md --filter pandoc-plantuml -o plantuml.pdf --template eisvogel.tex
docker run --rm   --entrypoint=solution-remove $1 -v