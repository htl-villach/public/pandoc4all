# Fahrzeuge

Implementieren Sie folgende Java-Klassen laut UML Diagramm und der weiteren
Beschreibung der Funktionalität. Die Werte für die Attribute sollen beim Erzeugen der Klasse
übergeben werden können. Für alle Attribute sind entsprechende Getter-Methoden zu implementieren. Bei Bedarf können natürlich weitere private-Methoden implementiert werden.

```{.plantuml width=60%}

enum AntriebsArt {
    FRONT
    HECK
    ALLRAD
}

class Fahrzeug {
  - modell: String
  - kennzeichen: String
  - zugelassenAm: LocalDate 
  - antrieb: AntriebsArt
  - leistungInkW: int
  

  + int monateZugelassen();
  + boolean steuerPflichtig();
  + double monatlicheSteuer();
  + String toString();
}

class DieselFahrzeug {
  - tankInhalt: int
}

class ElektroFahrzeug {
  - akkuKapazitaet: int
}

Fahrzeug <|-down- DieselFahrzeug
Fahrzeug <|-down- ElektroFahrzeug

```
