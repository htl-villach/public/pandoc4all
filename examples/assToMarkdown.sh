#!/bin/bash

if [ $# -ne 3 ]; then
  echo -e "Usage: $0 baseDir outFile regex\n"
  echo "Example: $0 /mnt/c/Temp/Abgabe14 abgabe14.md '.*SomeJavaFile(|Test)\.java'"
  exit 1
fi

BASEDIR=$1
OUTFILE=$2
REGEX=$3

echo "" >$OUTFILE

find "$BASEDIR" -mindepth 1 -maxdepth 1 -type d | sort | while read PROJDIR
do
 echo -e "\n# ${PROJDIR##*/}" >>$OUTFILE
 find "$PROJDIR" -regextype posix-egrep -regex "$REGEX" -type f -print | while read JAVAFILE

 do
   FILENAME=$(basename "$JAVAFILE")
   echo -e "\n## $FILENAME\n" >>$OUTFILE
   echo -e '\n```java'  >>$OUTFILE
   cat "$JAVAFILE" >>$OUTFILE
   echo -e '\n```\n' >>$OUTFILE
 done;

 echo -e "\n\n" >>$OUTFILE
done
